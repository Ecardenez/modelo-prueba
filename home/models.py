from django.db import models

# Create your models here.


class Produccion(models.Model):
    name_company = models.CharField(max_length=30)
    address = models.CharField(max_length=200)
    tel = models.IntegerField()
    email = models.EmailField(max_length=100)

    class Meta:
        verbose_name = 'Produccion'
        verbose_name_plural = 'Producciones'

    def __str__(self):
        return self.name_company


class Legajo(models.Model):
    file_number = models.IntegerField()
    cuil_company = models.IntegerField()
    addres_company = models.CharField(max_length=200)

    class Meta:
        verbose_name = 'Legajo'
        verbose_name_plural = 'Legajos'

    def __str_(self):
        return self.file_number


class Liquidacion(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    name_company = models.CharField(max_length=30)
    Liqui = models.IntegerField()

    class Meta:
        verbose_name = 'Liquidación'
        verbose_name_plural = 'Liquidaciones'

    def __str__(self):
        return self.date
