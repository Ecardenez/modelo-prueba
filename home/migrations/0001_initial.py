# Generated by Django 4.1.7 on 2023-03-15 21:24

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Legajo',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('file_number', models.IntegerField()),
                ('cuil_company', models.IntegerField()),
                ('addres_company', models.CharField(max_length=200)),
            ],
            options={
                'verbose_name': 'Legajo',
                'verbose_name_plural': 'Legajos',
            },
        ),
        migrations.CreateModel(
            name='Liquidacion',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('name_company', models.CharField(max_length=30)),
            ],
            options={
                'verbose_name': 'Liquidación',
                'verbose_name_plural': 'Liquidaciones',
            },
        ),
        migrations.CreateModel(
            name='Produccion',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name_company', models.CharField(max_length=30)),
                ('address', models.CharField(max_length=200)),
                ('tel', models.IntegerField()),
                ('email', models.EmailField(max_length=100)),
            ],
            options={
                'verbose_name': 'Produccion',
                'verbose_name_plural': 'Producciones',
            },
        ),
    ]
