from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout


from .models import Liquidacion, Produccion, Legajo


# Create your views here.
TEMPLATE_DIRS = (
       'os.path.join(BASE_DIR, "templates")'
)


@login_required
def home(request):
    return render(request, 'index.html')


def produccion(request):
    produccion = Produccion.objects.all()
    return render(request, "empresas/produccion.html", {"produccion": produccion})


def liquidacion(request):

    liquidacion = Liquidacion.objects.all()
    return render(request, "empresas/liquidacion.html", {"liquidacion": liquidacion})


def legajo(request):
    legajo = Legajo.objects.all()
    return render(request, "empresas/legajo.html", {"legajo": legajo})


def salir(request):
    logout(request)
    return redirect('/')
