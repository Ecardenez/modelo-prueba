from django.urls import path

from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('produccion/', views.produccion, name='produccion'),
    path('liquidacion/', views.liquidacion, name='liquidacion'),
    path('legajo/', views.legajo, name='legajo'),
    path('salir/', views.salir, name='salir'),
]
