from django.contrib import admin

from .models import Produccion, Legajo, Liquidacion


# Register your models here.


class Liquidacion_admin (admin.ModelAdmin):
    readonly_fields = ('date'),


admin.site.register(Produccion)


admin.site.register(Legajo)


admin.site.register(Liquidacion, Liquidacion_admin)

